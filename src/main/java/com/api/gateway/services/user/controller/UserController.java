package com.api.gateway.services.user.controller;

import com.api.gateway.services.user.dto.UserDto;
import com.api.gateway.services.user.dto.client.service.TokenServiceFeignClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.UUID;

@RestController
@RequestMapping(path = "users")
public class UserController {

    @Value(value = "${spring.application.name}")
    private String applicationName;

    @Autowired private TokenServiceFeignClientService tokenService;


    @GetMapping
    public ResponseEntity<?> getAll(){
        preAuth();
        String res =  "UserController -> getAll";
        System.out.println("************" + applicationName + "********************");
        System.out.println(">>>>>>>>>>>> UserController -> getAll <<<<<<<<<<<<<<<<");
        System.out.println("############" + res + "####################");
        return ResponseEntity.ok(res);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<?> getById(@PathVariable String id){
        preAuth();
        String res =  "UserController -> getById "+ id;
        System.out.println("************" + applicationName + "********************");
        System.out.println(">>>>>>>>>>>> UserController -> getById <<<<<<<<<<<<<<<<");
        System.out.println("############" + res + "####################");
        return ResponseEntity.ok(res);
    }

    @PostMapping
    public ResponseEntity<?> post(@RequestBody UserDto request){
        preAuth();
        request.setUuid(UUID.randomUUID());
        System.out.println("************" + applicationName + "********************");
        System.out.println(">>>>>>>>>>>> UserController -> post <<<<<<<<<<<<<<<<");
        System.out.println("############" + request.toString() + "####################");
        return ResponseEntity.ok(request);
    }

    private void preAuth(){
        System.out.println("############preAuth - validating ####################");
        Objects.requireNonNull(tokenService.getById());
        System.out.println("############preAuth - validation success ####################");
    }
}
