package com.api.gateway.services.user.dto;

import lombok.*;

import java.util.UUID;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor @Builder @ToString
public class TokenDto {
    private UUID uuid ;
    private String token;
}