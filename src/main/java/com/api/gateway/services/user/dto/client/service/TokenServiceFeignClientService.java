package com.api.gateway.services.user.dto.client.service;

import com.api.gateway.services.user.dto.TokenDto;
import com.api.gateway.services.user.dto.client.feign.TokenServiceFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class TokenServiceFeignClientService {

    @Autowired private TokenServiceFeignClient feignClient;

    public TokenDto getById(){
        ResponseEntity<TokenDto> response = feignClient.getById("12345");
        return Objects.requireNonNull(response).getBody();
    }
}
