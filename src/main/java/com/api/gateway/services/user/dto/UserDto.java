package com.api.gateway.services.user.dto;

import lombok.*;

import java.util.UUID;

@Getter
@Setter
@ToString
public class UserDto {

    private UUID uuid;
    private String string;
    private Integer integer;

}
