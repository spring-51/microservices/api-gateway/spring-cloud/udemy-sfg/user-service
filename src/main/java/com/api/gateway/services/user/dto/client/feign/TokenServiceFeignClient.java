package com.api.gateway.services.user.dto.client.feign;

import com.api.gateway.services.user.dto.TokenDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(
        name = "token-service" // this should be as same as application name of the target service (i.e spring.application.name)
        // , url = "http://token-service" // url will not work with eureka server, for eureka server name property is used
)
public interface TokenServiceFeignClient {

    @RequestMapping(
            method = RequestMethod.GET,
            path = "token-service/tokens/{id}"
    )
    ResponseEntity<TokenDto> getById(@PathVariable(name = "id")String id);
}
