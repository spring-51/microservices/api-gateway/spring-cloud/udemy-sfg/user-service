# User Service 

#### Pre Requisites
```
1. [OPTIONAL] - start api-gateway before this(its not optional if we are using  urls as per the gateway)
2. eureka server 
```

#### Note

``` 
1. I have created this service different from tutorial for 
my self knowledge. 

2. This service in independent, however its been integrtaed with
api-gateway service hence it can be invoked via api-gateway as well

```
#### Tutor
```
udemy/springframeworkguru/Spring Boot Micreoservices With Spring

```

#### integrating it with eureka server

```
Steps 

s1 - add dependency in pom
<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
		</dependency>

s2 -  add dependeny managemnet for spring-cloud dependency in pom
<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.springframework.cloud</groupId>
				<artifactId>spring-cloud-dependencies</artifactId>
				<version>${spring-cloud.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>
	
s3 - add <spring-cloud.version>2021.0.5</spring-cloud.version> inside <property> pom.xml

s4 - config application name in application.yml 
     - app will regiter it self on eureka with this name
spring:
  application:
    name: user-service
    

s5 - add @EnableDiscoveryClient on any config class(@Configuration annotated class)
   - I have added it on UserServiceApplication         
```

#### integrating feign client

```
Steps 

s1 - add dependency in pom
<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-openfeign</artifactId>
		</dependency>

s2 -  add dependeny managemnet for spring-cloud dependency in pom
<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.springframework.cloud</groupId>
				<artifactId>spring-cloud-dependencies</artifactId>
				<version>${spring-cloud.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>
	
s3 - add <spring-cloud.version>2021.0.5</spring-cloud.version> inside <property> pom.xml

s4 - add @EnableFeignClients on any config class(@Configuration annotated class)
   - I have added it on UserServiceApplication  
   
s5 - Add code changes to call REST apis to other service
   - TokenServiceFeignClient
   - TokenServiceFeignClientService
   - UserController -> preAuth    
```

#### Postman Collection

```
refer - gitlab/spring-51/microservices/api-gateway/spring-cloud/udemy-sfg/api-gateway 
        postman collection
      
```